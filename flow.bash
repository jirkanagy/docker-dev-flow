#!/usr/bin/env bash

##CURRENT DIR
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  TARGET="$(readlink "$SOURCE")"
  if [[ $TARGET == /* ]]; then
#    echo "SOURCE '$SOURCE' is an absolute symlink to '$TARGET'"
    SOURCE="$TARGET"
  else
    DIR="$( dirname "$SOURCE" )"
#    echo "SOURCE '$SOURCE' is a relative symlink to '$TARGET' (relative to '$DIR')"
    SOURCE="$DIR/$TARGET" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  fi
done
#echo "SOURCE is '$SOURCE'"
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

CMD=$1


if [ "${CMD}" == "build" ]; then

    docker-compose -f ./docker/docker-compose.yml build

    echo ""
    exit;

fi

if [ "${CMD}" == "clean" ]; then

    cd ${DIR}/docker

    docker-sync clean

    echo ""
    exit;

fi

if [ "${CMD}" == "exec" ]; then

    cd ${DIR}/docker

    docker-compose exec $2 sh

    echo ""
    exit;

fi

if [ "${CMD}" == "start" ]; then

    cd ${DIR}/docker

    docker-sync clean
    docker-sync start
    docker-compose up

    echo ""
    exit;

fi

if [ "${CMD}" == "stop" ]; then

    cd ${DIR}/docker

    docker-compose down
    docker-sync stop
    docker-sync clean

    echo ""
    exit;

fi

if [ "${CMD}" == "sync-log" ]; then

    cd ${DIR}/docker

    docker-sync log --follow

    echo ""
    exit;

fi




echo "WORKFLOW COMMAND FOR DEVELOPMENT WITH SYNC"
echo "------------------------------------------"
echo "Usage: bash flow.bash [CMD]"
echo ""
echo "Commands:"
echo "  build                # build services (docker-compose.yml)"
echo "  exec [serviceName]   # start interactive shell in selected service"
echo "  start                # start sync, start services"
echo "  stop                 # stop services, stop syncing"
echo "  sync-log             # follow syncing log"
echo ""