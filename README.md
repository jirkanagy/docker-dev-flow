# Example of development environment

This is example how effectively set up development environment for node applications

**This development stack using technologies:**

- docker
- node inspect
- docker-sync


## Instalation

... todo write description of docker-sync and docker-compose file...


## Development



### Run node application with inspect mode (debugger):
```
yarn start-with-inspect-mode
```

Console output (something like this):
```
yarn run v1.3.2
$ node --inspect=0.0.0.0:3333 server/index.js
Debugger listening on ws://0.0.0.0:3333/7fa1c0e7-2a09-4d95-8cbe-ce3ff5adc52c
For help, see: https://nodejs.org/en/docs/inspector


DONE  Compiled successfully in 2151ms
```
Important part is this:
```
3333/7fa1c0e7-2a09-4d95-8cbe-ce3ff5adc52c
```

Use this prefix and complete full url:
prefix:
```
chrome-devtools://devtools/bundled/inspector.html?experiments=true&v8only=true&ws=127.0.0.1:
```

complete url:
```
chrome-devtools://devtools/bundled/inspector.html?experiments=true&v8only=true&ws=127.0.0.1:3333/7fa1c0e7-2a09-4d95-8cbe-ce3ff5adc52c
```

Open complete url in Chrome browser.
Now you have node debbuger in docker container.
