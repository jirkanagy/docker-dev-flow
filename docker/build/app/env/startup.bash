#!/usr/bin/env bash

### INSTALATION
if [ ! "$(ls -A /application)" ]; then

     #download source code
     git clone https://jirkanagy@bitbucket.org/jirkanagy/docker-dev-flow.git /application-source

     #copy source code
     cp -r /application-source/src/* /application

     #install packages
     yarn install

fi

### STARTING

if [ "${DEBBUGER}" == 'true' ];then

    echo "start with debbuger"
    yarn start-with-inspect-mode

else

    echo "start"
    yarn start

fi

